<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Menampilkan Data</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery.dataTables.min.css" />
    
</head>
<body>
<?php
    $username="root";
    $pass="";

    //Koneksi db
    $conn = new PDO('mysql:server=localhost;dbname=universitas',$username,$pass);

    //Perintah sql
    $perintah = $conn->prepare("Select * from pendaftaran");
    //Eksekusi
    $perintah->execute();

    //Mengambil data tabel
    $getData = $perintah->fetchall();

?>

<h1>Daftar Pendaftaran</h1><hr>
    <table border='1'>
        <tr>
            <td>No</td>
            <td>Nama</td>
            <td>Alamat</td>
            <td>Telp</td>
            <td>JK</td>
            <td>Asal Sekolah</td>
            <td>Tempat Lhr</td>
            <td>Tgl Lahir</td>
            <td>Action</td>
        </tr>
        <?php foreach($getData as $row) { ?>
            <tr>
                <td><?php echo $row[0];  ?></td>
                <td><?php echo $row[1];  ?></td>
                <td><?php echo $row[2];  ?></td>
                <td><?php echo $row[3];  ?></td>
                <td><?php echo $row[4];  ?></td>
                <td><?php echo $row[5];  ?></td>
                <td><?php echo $row[6];  ?></td>
                <td><?php echo date('d M Y', strtotime($row[7]));  ?></td>
                <td>
                    <a href="#">EDIT</a>  |  
                    <a href="#">HAPUS</a> 
                </td>
            </tr>
        <?php } ?>
    </table>
    <hr>

<table id="dataku">
    <thead>
        <tr>
            <td>No</td>
            <td>Nama</td>
            <td>Alamat</td>
            <td>Telp</td>
            <td>JK</td>
            <td>Asal Sekolah</td>
            <td>Tempat Lhr</td>
            <td>Tgl Lahir</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($getData as $row) { ?>
            <tr>
                <td><?php echo $row[0];  ?></td>
                <td><?php echo $row[1];  ?></td>
                <td><?php echo $row[2];  ?></td>
                <td><?php echo $row[3];  ?></td>
                <td><?php echo $row[4];  ?></td>
                <td><?php echo $row[5];  ?></td>
                <td><?php echo $row[6];  ?></td>
                <td><?php echo date('d M Y', strtotime($row[7]));  ?></td>
                <td>
                    <a href="#">EDIT</a>  |  
                    <a href="#">HAPUS</a> 
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    
    <script>
    $(document).ready(function() {
        $('#dataku').DataTable();
    } );
    </script>
    
</body>
</html>