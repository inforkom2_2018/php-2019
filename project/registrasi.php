<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Flat Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
		<?php
			include('koneksi.php');
			
			$sql=$conn->prepare('Select COUNT(id) from pendaftaran');
			$sql->execute();
			
			$getData=$sql->fetchColumn();
			$autoNo="DFT-".$getData.date('dYm');
	
			
		?>
		<!-- Top menu -->
		<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div id="logo">
						<img class="logo" src="assets/img/logo.png">
						<h1>THE BEST OF UNIVERSITY</h1>
					</div>
				</div>
			</div>
		</nav>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
						<div class="col-md-2"></div>
                        <div class="col-sm-8 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Registrasi Data</h3>
                            		<p>Registrasi Calon Mahasiswa</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                        		<div class="form-top-divider"></div>
                            </div>
                            <div class="form-bottom">
								<?php if(isset($_GET['sukses'])){ 
									echo "<h1>SELAMAT<br>NO PENDAFTARAN : $autoNo<br>BERHASIL TERDAFTAR</h1>";
									echo "<h3><p>Silahkan LOGIN untuk melengkapi data.<br>Catat NO PENDAFTARAN dan Gunakan password yang telah didaftarkan.</p><h3>";
									echo "<div class='top-big-link'>";
									echo "	<a class='btn btn-link-1' href='index.php'>HALAMAN LOGIN</a>	";							
									echo "</div>";
									
								}else{?>
			                    <form role="form" action="simpan.php?reg=<?php echo $autoNo; ?>" method="post" class="registration-form">
									<div class="form-group">
			                    		
			                    		<h1>NO PENDAFTARAN : <?php echo $autoNo; ?></h1>
			                        	<input type="hidden" name="nodaftar" class="form-first-name form-control" value="<?php echo $autoNo; ?>">
			                        </div>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Nama Lengkap</label>
			                        	<input type="text" name="nama" placeholder="Nama Lengkap..." class="form-first-name form-control">
			                        </div>
									<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Password</label>
			                        	<input type="password" name="pass" placeholder="Password..." class="form-first-name form-control">
			                        </div>
			                        <button type="submit" class="btn">DAFTAR !</button>
			                    </form>
								<?php } ?>
		                    </div>
                        </div>
						<div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>