<?php
session_start();
$base = "http://localhost/ik/";

require_once 'vendor/autoload.php';
include('koneksi.php');
$sql=$conn->prepare("Select * from pendaftaran where nodaftar='".$_SESSION['login']."'");
$sql->execute();
$getData=$sql->fetch();
ob_start();
?>
<style>
.biodata { position: relative; }
.biodata table { position: relative; width: 100%; border-collapse:collapse; }
.biodata table td { position: relative; border-collapse:collapse; padding: 5px 10px;  }
.biodata table tr td:nth-child(1) { background: #5F3253; font-weight: bold; color: #DAB3CE; text-align: left; text-transform: capitalize; }
.biodata table tr td:nth-child(2) {  background: #5F3253; font-weight: bold; color: #fff; }
.biodata table tr td:nth-child(3) {  background: #946D88; color: #fff; text-align: left; }
.biodata .btn  { display: block; background: #11772d; font-size: 30px; text-align: center; color: #f7e771; }
img { width:350px; }
</style>
<div class="biodata">
<table>
	<tr>
		<td width="25%">NO PENDAFTARAN</td>
		<td width="3%">:</td>
		<td width="62%"><?php echo $getData['nodaftar']; ?></td>
	</tr>
	<tr>
		<td>Nama Lengkap</td>
		<td>:</td>
		<td><?php echo $getData['nama_lengkap']; ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td><?php echo $getData['alamat']; ?></td>
	</tr>
	<tr>
		<td>No Telp</td>
		<td>:</td>
		<td><?php echo $getData['telp']; ?></td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td>:</td>
		<td><?php echo $getData['jk']; ?></td>
	</tr>
	<tr>
		<td>Asal Sekolah</td>
		<td>:</td>
		<td><?php echo $getData['asal_sekolah']; ?></td>
	</tr>
	<tr>
		<td>Tempat, Tgl Lahir</td>
		<td>:</td>
		<td><?php echo $getData['tmpt_lhr'].", ".date("d-M-F",strtotime($getData['tgl_lhr'])); ?></td>
	</tr>
	<tr>
		<td>Agama</td>
		<td>:</td>
		<td><?php echo $getData['agama']; ?></td>
	</tr>
	<tr>
		<td valign="top">Foto</td>
		<td valign="top">:</td>
		<td><img src="<?php echo $base."file/".$getData['foto']; ?>" ></td>
	</tr>
</table>
</div>
<?php

$content = ob_get_clean();
$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($content);
$mpdf->Output();
?>