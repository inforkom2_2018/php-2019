-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.33-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for universitas
DROP DATABASE IF EXISTS `universitas`;
CREATE DATABASE IF NOT EXISTS `universitas` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `universitas`;

-- Dumping structure for table universitas.pendaftaran
DROP TABLE IF EXISTS `pendaftaran`;
CREATE TABLE IF NOT EXISTS `pendaftaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nodaftar` varchar(50) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `asal_sekolah` varchar(50) DEFAULT NULL,
  `tmpt_lhr` varchar(50) DEFAULT NULL,
  `tgl_lhr` date DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.pendaftaran: ~2 rows (approximately)
/*!40000 ALTER TABLE `pendaftaran` DISABLE KEYS */;
INSERT INTO `pendaftaran` (`id`, `nodaftar`, `pass`, `nama_lengkap`, `alamat`, `telp`, `jk`, `asal_sekolah`, `tmpt_lhr`, `tgl_lhr`, `agama`, `foto`, `status`) VALUES
	(5, 'DFT-031201901', '21232f297a57a5a743894a0e4a801fc3', 'Indra Laksana Putra', NULL, NULL, NULL, NULL, NULL, '2019-02-04', NULL, 'DFT-031201901.jpg', NULL),
	(6, 'DFT-131201901', '21232f297a57a5a743894a0e4a801fc3', 'Arin F', 'Jl. Thamrin 35 A Madiun', '0815525454', 'Pria', 'SMA 1 Madiun', 'Madiun', '2019-02-04', 'Islam', '4.Mode desain Form Data Kasir.jpg', NULL);
/*!40000 ALTER TABLE `pendaftaran` ENABLE KEYS */;

-- Dumping structure for table universitas.t_admin
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE IF NOT EXISTS `t_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.t_admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_admin` ENABLE KEYS */;

-- Dumping structure for table universitas.t_dosen
DROP TABLE IF EXISTS `t_dosen`;
CREATE TABLE IF NOT EXISTS `t_dosen` (
  `nip` int(11) NOT NULL,
  `nama` char(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.t_dosen: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_dosen` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_dosen` ENABLE KEYS */;

-- Dumping structure for table universitas.t_matkul
DROP TABLE IF EXISTS `t_matkul`;
CREATE TABLE IF NOT EXISTS `t_matkul` (
  `kd_matkul` char(50) NOT NULL,
  `nama_matkul` char(50) DEFAULT NULL,
  PRIMARY KEY (`kd_matkul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.t_matkul: ~5 rows (approximately)
/*!40000 ALTER TABLE `t_matkul` DISABLE KEYS */;
INSERT INTO `t_matkul` (`kd_matkul`, `nama_matkul`) VALUES
	('MK1', 'C#'),
	('MK2', 'MS EXCEL'),
	('MK3', 'MS WORD'),
	('MK4', 'PAJAK'),
	('MK5', 'MS ACCESS');
/*!40000 ALTER TABLE `t_matkul` ENABLE KEYS */;

-- Dumping structure for table universitas.t_mhs
DROP TABLE IF EXISTS `t_mhs`;
CREATE TABLE IF NOT EXISTS `t_mhs` (
  `nim` char(50) NOT NULL,
  `nama` char(50) DEFAULT NULL,
  `kelas` char(50) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.t_mhs: ~9 rows (approximately)
/*!40000 ALTER TABLE `t_mhs` DISABLE KEYS */;
INSERT INTO `t_mhs` (`nim`, `nama`, `kelas`) VALUES
	('123', 'Sapto', 'KS3'),
	('2018320999', 'Indra laksana putra', 'IK2'),
	('20189', 'jojo', 'ks2'),
	('234', 'ANDIN', 'IK2'),
	('345', 'DITA', 'KS1'),
	('456', 'ADEL', 'KE1'),
	('567', 'DENI', 'KE2'),
	('652388', 'lapo', 'dk1'),
	('678', 'KOKO', 'DK1');
/*!40000 ALTER TABLE `t_mhs` ENABLE KEYS */;

-- Dumping structure for table universitas.t_nilai
DROP TABLE IF EXISTS `t_nilai`;
CREATE TABLE IF NOT EXISTS `t_nilai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` char(50) DEFAULT NULL,
  `kd_matkul` char(50) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table universitas.t_nilai: ~4 rows (approximately)
/*!40000 ALTER TABLE `t_nilai` DISABLE KEYS */;
INSERT INTO `t_nilai` (`id`, `nim`, `kd_matkul`, `nilai`) VALUES
	(1, '123', 'MK1', 80),
	(2, '123', 'MK2', 90),
	(3, '234', 'MK3', 50),
	(4, '456', 'MK2', 75);
/*!40000 ALTER TABLE `t_nilai` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
