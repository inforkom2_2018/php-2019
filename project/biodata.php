<!DOCTYPE html>
<html lang="en">
<?php 
	session_start();
	if(!isset($_SESSION["login"]))
	{
		header('location:index.php');
	}
	
?>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Flat Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

		<!-- Top menu -->
		<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<div id="logo">
						<img class="logo" src="assets/img/logo.png">
						<h1>THE BEST OF UNIVERSITY</h1>
					</div>
				</div>
			</div>
		</nav>
		
		<div class="biomenu">
			<div class="row">
				<div class="col-md-6 mn"><a href="biodata.php">BIODATA</a></div>
				<div class="col-md-6 mn"><a href="?edit=<?php echo $_SESSION['login']; ?>">RUBAH BIODATA</a></div>
			</div>
		</div>
		
		<?php 
			include('koneksi.php');
			
			if(isset($_GET['logout']))
			{
				session_destroy();
				header('location:index.php');
			}
			
			$sql=$conn->prepare("Select * from pendaftaran where nodaftar='".$_SESSION['login']."'");
			$sql->execute();
			$getData=$sql->fetch();
				
			//Biodata
			if(!isset($_GET['edit']))
			{
				
		?>
		<div class="biodata">
			<table>
				<tr>
					<td width="25%">NO PENDAFTARAN</td>
					<td width="3%">:</td>
					<td width="62%"><?php echo $getData['nodaftar']; ?></td>
				</tr>
				<tr>
					<td>Nama Lengkap</td>
					<td>:</td>
					<td><?php echo $getData['nama_lengkap']; ?></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td><?php echo $getData['alamat']; ?></td>
				</tr>
				<tr>
					<td>No Telp</td>
					<td>:</td>
					<td><?php echo $getData['telp']; ?></td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td><?php echo $getData['jk']; ?></td>
				</tr>
				<tr>
					<td>Asal Sekolah</td>
					<td>:</td>
					<td><?php echo $getData['asal_sekolah']; ?></td>
				</tr>
				<tr>
					<td>Tempat, Tgl Lahir</td>
					<td>:</td>
					<td><?php echo $getData['tmpt_lhr'].", ".date("d-M-Y",strtotime($getData['tgl_lhr'])); ?></td>
				</tr>
				<tr>
					<td>Agama</td>
					<td>:</td>
					<td><?php echo $getData['agama']; ?></td>
				</tr>
				<tr>
					<td valign="top">Foto</td>
					<td valign="top">:</td>
					<td><img src="file/<?php echo $getData['foto']; ?>"</td>
				</tr>
				<tr>
					<td colspan="3"><a class="btn" href="report.php" target="blank">CETAK DATA</a></td>
				</tr>
				<tr>
					<td colspan="3"><a class="btn" href="?logout=ok" target="blank">LOGOUT</a></td>
				</tr>
				
				
			</table>
		</div>
			
		<?php
			}else{
			//Jika Rubah Data	
				
								
		?>
        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
						<div class="col-md-2"></div>
                        <div class="col-sm-8 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>BIODATA</h3>
                            		<p>Lengkapi biodata dan dokumen anda untuk pendaftaran</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-pencil"></i>
                        		</div>
                        		<div class="form-top-divider"></div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="simpan.php?upd=<?php echo $_SESSION['login']; ?>" method="post" class="registration-form" enctype="multipart/form-data">
								<?php 
									echo "<h1>NO PENDAFT : ".$_SESSION["login"];
									
								?>
			                    	<div class="form-group">
			                    		<label class="sr-only">Nama Lengkap</label>
										<input type="hidden" name="no" class="form-control" value="<?php echo $_SESSION['login']; ?>">
			                        	<input type="text" name="nama" placeholder="Nama Lengkap" class="form-first-name form-control" value="<?php echo $getData['nama_lengkap']; ?>">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only">Alamat Lengkap</label>
			                        	<input type="text" name="alamat" placeholder="Alamat Lengkap..." class="form-control" value="<?php echo $getData['alamat']; ?>">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only">Telp</label>
			                        	<input type="text" name="telp" placeholder="Telp..." class="form-control" value="<?php echo $getData['telp']; ?>">
			                        </div>
									<div class="form-group rd">
			                        	<label class="sr-only">Jenis Kelamin</label>
			                        	  <input type="radio" name="jk" value="Pria" <?php echo $getData['jk']=="Pria" ? "checked" : ""; ?>><span>Laki-Laki</span>
										  <input type="radio" name="jk" value="Wanita" <?php echo $getData['jk']=="Wanita" ? "checked" : ""; ?>><span>Perempuan</span>
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only">Asal Sekolah</label>
			                        	<input type="text" name="asal" placeholder="Asal Sekolah" class="form-control" value="<?php echo $getData['asal_sekolah']; ?>">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only">Tempat Tanggal Lahir</label>
			                        	<div class="row">
											<div class="col-md-6">
												<input type="text" name="tmpt_lhr" placeholder="Tempat Lahir" class="form-control" value="<?php echo $getData['tmpt_lhr']; ?>">
											</div>
											<div class="col-md-6">
												<input type="date" name="tgl_lhr" placeholder="Tanggal Lahir" class="form-control" value="<?php echo $getData['tgl_lhr']; ?>">
											</div>
										</div>
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only">Agama</label>
			                        	<select name="agm" class="field-select" required>
											<option value="">- Pilih Agama -</option>
											<option value="Islam" <?php echo $getData['agama']=="Islam" ? "selected" : ""; ?>>Islam</option>
											<option value="Kristen" <?php echo $getData['agama']=="Kristen" ? "selected" : ""; ?> >Kristen</option>
											<option value="Katolik" <?php echo $getData['agama']=="Katolik" ? "selected" : ""; ?> >Katolik</option>
											<option value="Hindu" <?php echo $getData['agama']=="Hindu" ? "selected" : ""; ?>  >Hindu</option>
											<option value="Budha" <?php echo $getData['agama']=="Budha" ? "selected" : ""; ?> >Budha</option>
										</select>

			                        </div>
									<div class="form-group">
										<div class="upload">
											<input type="file" name="foto" class="form-control">
											<label><i class="fa fa-upload"></i> UPLOAD FILE</label>
											<img id="avatar" src="">
										</div>
			                        </div>
									
			                        <button type="submit" class="btn">Simpan Data</button>
			                    </form>
								
		                    </div>
                        </div>
						<div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            
        </div>
		<!-- End Top content -->
			<?php } ?>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>