<!DOCTYPE html>
<html lang="en">
<?php session_start(); ?>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Flat Registration Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

		<!-- Top menu -->
		<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div id="logo">
						<img class="logo" src="assets/img/logo.png">
						<h1>THE BEST OF UNIVERSITY</h1>
					</div>
				</div>
			</div>
		</nav>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text">
							<h1><strong>INFORMASI</strong> PENDAFTARAN UNIVERSITAS</h1>
							<div class="description">
								<p>Jadwal Pendaftaran dan Seleksi Mahasiswa Baru 2019/2020<br>Jalur Prestasi (PMDK, PSB, PBUP, dan PNUAN)</p>
								
								<table width="100%" border="1">
									<thead class="judul">
									
									<tr>
										<td style="text-align: center;" width="100"><span style="color: #000000;"><strong>JALUR</strong></span></td>
										<td style="text-align: center;" width="200"><span style="color: #000000;"><strong>PENDAFTARAN</strong></span></td>
										<td style="text-align: center;" width="168"><span style="color: #000000;"><strong>PENGUMUMAN</strong></span></td>
									
										<td style="text-align: center;" width="167"><span style="color: #000000;"><strong>REGISTRASI</strong></span></td>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td width="100"><strong><span style="color: #fff;">Periode 1</span></strong></td>
										<td width="200"><span style="color: #fff;">26 Nov 2018 – 1 Feb 2019</span></td>
										<td width="168"><span style="color: #fff;">13 Feb 2019</span></td>
										<td width="167"><span style="color: #fff;">29 Apr – 10 Mei 2019</span></td>
									</tr>
									<tr>
										<td width="100"><strong><span style="color: #fff;">Periode 2</span></strong></td>
										<td width="200"><span style="color: #fff;">4 Feb – 15 Mar 2019</span></td>
										<td width="168"><span style="color: #fff;">27 Mar 2019</span></td>
										<td width="167"><span style="color: #fff;">6 – 17 Mei 2019</span></td>
									</tr>
									<tr>
										<td width="100"><strong><span style="color: #fff;">Periode 3</span></strong></td>
										<td width="200"><span style="color: #fff;">18 Maret – 18 Apr 2019</span></td>
										<td width="168"><span style="color: #fff;">3 Mei 2019</span></td>
										<td width="167"><span style="color: #fff;">20 Mei – 21 Jun 2019</span></td>
									</tr>
									
									</tbody>
								</table>
								
								
							</div>
							<div class="top-big-link">
								<a class="btn btn-link-1" href="registrasi.php">REGISTRASI NOW !</a>
								<a class="btn btn-link-1" href="#">DAFTAR MAHASISWA DITERIMA</a>								
							</div>
                        </div>
                        <div class="col-sm-5 form-box">
                        	                       <div class="form-box-login">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>LOGIN</h3>
									<p>Login untuk melengkapi BIODATA anda</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                        		<div class="form-top-divider"></div>
                            </div>
							
							
                            <div class="form-bottom">
			                    <form role="form" action="login_cek.php" method="post" class="registration-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">No Pendaftaran</label>
			                        	<input type="text" name="nomor" placeholder="No Pendaftaran" class="form-first-name form-control">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">Password</label>
			                        	<input type="password" name="pass" placeholder="Password" class="form-last-name form-control">
			                        </div>
			                       
			                        <button type="submit" class="btn">LOGIN</button>
			                    </form>
		                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>