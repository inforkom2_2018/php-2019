<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    
</head>
<body>
  <?php
    include("koneksi.php");

    if(isset($_GET['edit'])){
        $perintah=$conn->prepare("select * from pendaftaran where id=".$_GET['edit']);
        $perintah->execute();

        $getData = $perintah->fetch();
    }
    else
    {
        $getData=array("id"=>"","nama_lengkap"=>"","alamat"=>"","telp"=>"","jk"=>"","asal_sekolah"=>"","tmpt_lhr"=>"","tgl_lhr"=>"","agama"=>"");
    }
    


  ?>
    <form action="simpan.php<?php echo "?sedit=".$_GET['edit']; ?>" method="POST" enctype="multipart/form-data">
    <ul class="form-style-1">
        <li><label>Nama Lengkap <span class="required">*</span></label>
            <input type="text" name="nama" class="field-long" placeholder="Nama Lengkap" value="<?php echo $getData["nama_lengkap"];  ?>"/> 
        </li>
        <li>
            <label>Alamat Lengkap <span class="required">*</span></label>
            <textarea name="almt" class="field-long field-textarea" placeholder="Alamat Lengkap" > <?php echo $getData["alamat"];  ?></textarea>
            
        </li>
        <li><label>No Telp</label>
            <input type="text" name="telp" class="field-long" placeholder="No Telp" value="<?php echo $getData['telp']; ?>"/> 
        </li>
        <li><label>Jenis Kelamin</label>
                <input type="radio" name="jk" value="Pria" <?php echo $getData['jk']=="Pria" ? "checked" : ""; ?>>Laki-Laki</input>
                <input type="radio" name="jk" value="Wanita" <?php echo $getData['jk']=="Wanita" ? "checked" : ""; ?>>Perempuan</input>
        </li>
        <li><label>Asal Sekolah</label>
            <input type="text" name="asal" class="field-long" placeholder="Asal Sekolah" /> 
        </li>
        <li><label>Tempat Lahir</label>
            <input type="text" name="tmptlhr" class="field-divided" placeholder="Tempat Lahir"/> 
        </li>
        <li><label>Tanggal Lahir</label>
            <input type="date" name="tgllhr" class="field-divided" /> 
        </li>
        <li>
    
            <label>Subject</label>
            <select name="agm" class="field-select">
                <option value="Islam" <?php echo $getData['agama']=="Islam" ? "selected" : ""; ?>>Islam</option>
                <option value="Kristen" <?php echo $getData['agama']=="Kristen" ? "selected" : ""; ?> >Kristen</option>
                <option value="Katolik" <?php echo $getData['agama']=="Katolik" ? "selected" : ""; ?> >Katolik</option>
                <option value="Hindu" <?php echo $getData['agama']=="Hindu" ? "selected" : ""; ?>  >Hindu</option>
                <option value="Budha" <?php echo $getData['agama']=="Budha" ? "selected" : ""; ?> >Budha</option>
            </select>
        </li>
        <li><label>Foto</label>
            <input type="file" name="foto"/> 
        </li>
        <li>
            <input type="submit" value="Submit" />
        </li>
    </ul>
    </form>

</body>
</html>